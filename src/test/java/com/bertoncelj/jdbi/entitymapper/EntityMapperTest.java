/*
Copyright 2016 Rok Bertoncelj

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.bertoncelj.jdbi.entitymapper;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Date;

import static org.testng.Assert.*;

/**
 * @author Rok Bertoncelj
 */
public class EntityMapperTest {
    private Handle handle;

    @BeforeClass
    public void loadDriver() throws ClassNotFoundException {
        Class.forName("org.h2.Driver");
    }

    @BeforeMethod
    public void setUp() throws Exception {
        handle = DBI.open("jdbc:h2:mem:");
    }

    @AfterMethod
    public void tearDown() throws Exception {
        handle.close();
    }

    @Test
    public void testDefaultMapping() throws Exception {
        handle.update("" +
                "CREATE TABLE foo (\n" +
                "  id      CHARACTER VARYING(36),\n" +
                "  created TIMESTAMP(6),\n" +
                "  enabled BOOLEAN,\n" +
                "  PRIMARY KEY (id)\n" +
                ");");
        handle.update("INSERT INTO foo (id, created, enabled) VALUES ('abc123', now(), 'true')");
        Foo foo = handle.createQuery("SELECT * FROM foo")
                .map(new EntityMapper<Foo>(Foo.class))
                .first();
        assertEquals(foo.getId(), "abc123");
        assertNotNull(foo.getCreated());
        assertTrue(foo.getEnabled());
    }

    @Test
    public void testOverridenMapping() throws Exception {
        handle.update("" +
                "CREATE TABLE bar (\n" +
                "  bar_id               CHARACTER VARYING(36),\n" +
                "  when_was_bar_created TIMESTAMP(6),\n" +
                "  is_bar_enabled       BOOLEAN,\n" +
                "  PRIMARY KEY (bar_id)\n" +
                ");");
        handle.update("INSERT INTO bar (bar_id, when_was_bar_created, is_bar_enabled) VALUES ('abc123', now(), 'true')");
        Bar bar = handle.createQuery("SELECT * FROM bar")
                .map(new EntityMapper<Bar>(Bar.class))
                .first();
        assertEquals(bar.getId(), "abc123");
        assertNotNull(bar.getCreated());
        assertTrue(bar.getEnabled());
    }

    public static class Foo {
        private String id;
        private Date created;
        private Boolean enabled;

        public Foo() {
        }

        public Foo(String id, Date created, Boolean enabled) {
            this.id = id;
            this.created = created;
            this.enabled = enabled;
        }

        public String getId() {
            return id;
        }

        public Date getCreated() {
            return created;
        }

        public Boolean getEnabled() {
            return enabled;
        }
    }

    public static class Bar {
        @Column("bar_id")
        private String id;
        @Column("when_was_bar_created")
        private Date created;
        @Column("is_bar_enabled")
        private Boolean enabled;

        public Bar() {
        }

        public Bar(String id, Date created, Boolean enabled) {
            this.id = id;
            this.created = created;
            this.enabled = enabled;
        }

        public String getId() {
            return id;
        }

        public Date getCreated() {
            return created;
        }

        public Boolean getEnabled() {
            return enabled;
        }
    }
}