# jdbi-entity-mapper

Automatically map SQL query results to objects by (optionally) annotating fields with column names.

Tested with JDBI 2.63.1

## Example usage

### 1. Example database table
```sql
CREATE TABLE user_account (
  id           TEXT,
  created_date TIMESTAMP,
  enabled      BOOLEAN,
  email        TEXT,
  PRIMARY KEY (id)
);
```

### 2. Add maven dependency
```xml
<dependency>
    <groupId>com.bertoncelj.jdbi.entitymapper</groupId>
    <artifactId>jdbi-entity-mapper</artifactId>
    <version>1.0.0</version>
</dependency>
```

### 3. Map fields to columns
Annotate fields of entity class where necessary:
```java
public class UserAccount {
    // use field name by default
    private String id;

    // overridden column name
    @Column("created_date")
    private Date created;

    // use field name by default
    private Boolean enabled;

    // overridden column name
    @Column("email")
    private String emailAddress;
}
```

### 4. Do some mapping
Use ``EntityMapper`` with a query:
```java
UserAccount userAccount = handle.createQuery("SELECT * FROM user_account")
    .map(new EntityMapper<UserAccount>(UserAccount.class))
    .first();
```

Or use ``EntityMapperFactory`` on a DAO:
```java
@RegisterMapperFactory(EntityMapperFactory.class)
public interface UserAccountDao {
    @SqlQuery("SELECT * FROM user_account WHERE id = :id")
    UserAccount findById(@Bind("id") String id);
}
```
